package com.alirhn.assignment.ui.state

import com.alirhn.assignment.baseClasses.BaseUIState
import com.alirhn.assignment.remote.model.ToDos

data class ToDosUIState(
    val toDosList: BaseUIState<ToDos> = BaseUIState(),
)
