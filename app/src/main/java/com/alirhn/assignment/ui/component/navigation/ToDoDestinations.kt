package com.alirhn.assignment.ui.component.navigation

object ToDoDestinations {
    const val LOGIN_ROUTE = ToDosScreen.LOGIN
    const val TODOS_LIST_ROUTE =
        "${ToDosScreen.TODOS_LIST_SCREEN}/{${TodosDestinationsArgs.USER_ID_ARG}}"
}