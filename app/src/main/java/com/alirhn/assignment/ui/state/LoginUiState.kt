package com.alirhn.assignment.ui.state

import com.alirhn.assignment.baseClasses.BaseUIState
import com.alirhn.assignment.remote.model.LoginResponse

data class LoginUiState(
    val loginResponse: BaseUIState<LoginResponse> = BaseUIState(),
    )
