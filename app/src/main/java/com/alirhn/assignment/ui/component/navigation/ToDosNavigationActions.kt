package com.alirhn.assignment.ui.component.navigation

import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import com.alirhn.assignment.ui.component.navigation.ToDosScreen.LOGIN
import com.alirhn.assignment.ui.component.navigation.ToDosScreen.TODOS_LIST_SCREEN


class ToDosNavigationActions(private val navController: NavHostController) {



    fun navigatesToTodosList(userId : String) {
        navController.navigate(
            route = "$TODOS_LIST_SCREEN/$userId"
        ){
            this.popUpTo(id = navController.graph.findStartDestination().id) {
                inclusive = true
                saveState = true
            }
            launchSingleTop = true
        }
    }

    fun navigatesToLogin() {
        navController.navigate(
            route = LOGIN
        )
    }


}