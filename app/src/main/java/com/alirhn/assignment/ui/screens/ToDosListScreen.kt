package com.alirhn.assignment.ui.screens

import ToDosDefaultErrorView
import ToDosDefaultLoading
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Checkbox
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.alirhn.assignment.R
import com.alirhn.assignment.remote.model.ToDosItem
import com.alirhn.assignment.remote.model.ToDos
import com.alirhn.assignment.ui.state.ToDosUIState


@Composable
fun TodosListScreen(
    state: ToDosUIState,
    ) {

    val showLoading by remember(
        state.toDosList.isLoading,
    ) {
        derivedStateOf {
            state.toDosList.isLoading
        }
    }

    val showData by remember(
        state.toDosList.data,
    ) {
        derivedStateOf {
            state.toDosList.data != null
        }
    }

    val showError by remember(
        state.toDosList.errorMessage,
    ) {
        derivedStateOf {
            state.toDosList.errorMessage != null
        }
    }

    when {
        showLoading -> {
            ToDosDefaultLoading()
        }

        showData -> {
            state.toDosList.data?.let { ToDos ->
                ToDoListViewContents(
                    ToDos = ToDos,
                )
            }
        }

        showError -> {
            state.toDosList.errorMessage?.let { ToDosDefaultErrorView(it) }
        }
    }


}

@Composable
fun ToDoListViewContents(
    ToDos: ToDos,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(
            items = ToDos,
            key = { it.id }
        ) { ToDo ->
            ToDoItem(
                todo = ToDo,
            )
        }
    }
}

@Composable

fun ToDoItem(
    todo: ToDosItem,
) {

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = 8.dp,
                vertical = 8.dp
            ),
        shape = RoundedCornerShape(corner = CornerSize(16.dp)),
        border = BorderStroke(
            width = 1.dp,
            color = if(todo.completed) Color.Cyan else Color.Red
        )
    ) {
        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .height(100.dp)
                .background(color = MaterialTheme.colorScheme.background)
        ) {

            val (titleText, idText, done) = createRefs()



            Text(
                modifier = Modifier
                    .constrainAs(titleText) {
                        start.linkTo(done.start)
                        end.linkTo(parent.end)
                        top.linkTo(parent.top)
                        width = Dimension.fillToConstraints
                    }
                    .padding(horizontal = 8.dp)
                    .padding(top = 8.dp),
                text = todo.title,
                color = MaterialTheme.colorScheme.onBackground,
                fontSize = 18.sp,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
            )

            Text(
                modifier = Modifier
                    .constrainAs(idText) {
                        start.linkTo(done.end)
                        end.linkTo(parent.end)
                        top.linkTo(titleText.bottom)
                        width = Dimension.fillToConstraints
                    }
                    .padding(horizontal = 8.dp)
                    .padding(top = 8.dp),
                text = stringResource(id = R.string.id) + " : " + todo.id.toString(),
                color = MaterialTheme.colorScheme.onBackground,
                fontSize = 18.sp,
            )

            Checkbox(
                checked = todo.completed,
                onCheckedChange = {

                },
                modifier = Modifier.constrainAs(done) {
                    start.linkTo(parent.start)
                    top.linkTo(titleText.bottom)
                })

        }
    }

}
