package com.alirhn.assignment.ui.component.navigation

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.alirhn.assignment.presentation.LoginViewModel
import com.alirhn.assignment.presentation.TodosViewModel
import com.alirhn.assignment.ui.screens.LoginScreen
import com.alirhn.assignment.ui.screens.TodosListScreen
import kotlinx.coroutines.CoroutineScope

@Composable
fun TodosNavGraph(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    startDestination: String = ToDoDestinations.LOGIN_ROUTE,
    navAction: ToDosNavigationActions = remember(navController) {
        ToDosNavigationActions(navController)
    }
) {


    NavHost(
        modifier = modifier, navController = navController, startDestination = startDestination
    ) {

        loginScreen(
            builder = this,
            navAction = navAction,
        )

        todosListScreen(
            builder = this,
            navAction = navAction,
        )

    }

}

fun loginScreen(
    builder: NavGraphBuilder,
    navAction: ToDosNavigationActions,
) {
    builder.apply {
        composable(
            route = ToDoDestinations.LOGIN_ROUTE
        ) { entry ->
            val viewModel = hiltViewModel<LoginViewModel>()
            val state by viewModel.uiState.collectAsState()

            LoginScreen(
                navigateToNextScreen = {userId->
                    navAction.navigatesToTodosList(userId)
                },
                state,
                viewModel
            )
        }
    }
}

fun todosListScreen(
    builder: NavGraphBuilder,
    navAction: ToDosNavigationActions,
) {
    builder.apply {
        composable(
            route = ToDoDestinations.TODOS_LIST_ROUTE
        ) { entry ->

            val viewModel = hiltViewModel<TodosViewModel>()
            val state by viewModel.uiState.collectAsState()

            TodosListScreen(
                state = state,
            )
        }
    }
}




