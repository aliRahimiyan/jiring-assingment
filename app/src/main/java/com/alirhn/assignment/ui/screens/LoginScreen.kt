package com.alirhn.assignment.ui.screens

import ToDosDefaultErrorView
import ToDosDefaultLoading
import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.alirhn.assignment.R
import com.alirhn.assignment.presentation.LoginViewModel
import com.alirhn.assignment.ui.state.LoginUiState
import com.alirhn.assignment.ui.state.ToDosUIState
import kotlin.math.log

@Composable
fun LoginScreen(
    navigateToNextScreen: (String) -> Unit,
    state: LoginUiState,
    viewModel: LoginViewModel
) {

    LoginScreenContent(viewModel)

    val showLoading by remember(
        state.loginResponse.isLoading,
    ) {
        derivedStateOf {
            state.loginResponse.isLoading
        }
    }

    val showData by remember(
        state.loginResponse.data,
    ) {
        derivedStateOf {
            state.loginResponse.data != null
        }
    }

    val showError by remember(
        state.loginResponse.errorMessage,
    ) {
        derivedStateOf {
            state.loginResponse.errorMessage != null
        }
    }

    when {
        showLoading -> {
            ToDosDefaultLoading()
        }

        showData -> {
            state.loginResponse.data?.let { login ->
                if (login.isNotEmpty()) {
                    navigateToNextScreen(login[0].id.toString());
                } else {
                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier.padding(top = 40.dp)
                    ) {
                        Text(
                            text = stringResource(id = R.string.user_not_exists),
                            color = Color.Red,
                            fontWeight = FontWeight.ExtraBold
                        )
                    }
                }
            }
        }

        showError -> {
            state.loginResponse.errorMessage?.let { ToDosDefaultErrorView(it) }
        }
    }
}

@Composable
fun LoginScreenContent(viewModel: LoginViewModel) {
    var username by remember { mutableStateOf("") }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            modifier = Modifier.padding(8.dp),
            text = stringResource(id = R.string.login),
            color = MaterialTheme.colorScheme.onBackground,
            fontSize = 18.sp,
        )
        Text(
            modifier = Modifier.padding(8.dp),
            text = stringResource(id = R.string.enter_your_username),
            color = Color.Green, fontSize = 14.sp, fontWeight = FontWeight.Bold
        )

        TextField(
            value = username,
            onValueChange = {
                username = it
            },
            shape = RoundedCornerShape(corner = CornerSize(8.dp)),
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done,
            ),
            placeholder = { Text(text = stringResource(id = R.string.enter_your_username)) },
            singleLine = true
        )

        Spacer(modifier = Modifier.height(60.dp))

        Button(onClick = {
            viewModel.login(username = username)
        }) {
            Text("Login")
        }
    }
}





