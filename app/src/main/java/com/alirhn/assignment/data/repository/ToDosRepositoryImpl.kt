package com.alirhn.assignment.data.repository

import com.alirhn.assignment.data.dataStore.ToDosRemoteDataStore
import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos
import com.alirhn.assignment.utils.apiCall
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ToDosRepositoryImpl @Inject constructor(
    private val toDosDataStore: ToDosRemoteDataStore
) : ToDosRepository{

    override suspend fun getToDosList(userId: String): Flow<Result<ToDos>> = flow {
        val result = apiCall { toDosDataStore.getToDos(userId) }
        result.onSuccess {todos->
            emit(Result.success(todos))
        }
        result.onFailure {failure->
            emit(Result.failure(failure))
        }
    }

    override suspend fun login(username: String): Flow<Result<LoginResponse>> = flow {
        val result = apiCall { toDosDataStore.login(username) }
        result.onSuccess { loginResult ->
            emit(Result.success(loginResult))
        }
        result.onFailure { failure->
            emit(Result.failure(failure))
        }
    }
}