package com.alirhn.assignment.data.source

import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos

interface ToDosRemoteSource {

    suspend fun getToDos(userId : String): ToDos

    suspend fun login(username : String) : LoginResponse
}