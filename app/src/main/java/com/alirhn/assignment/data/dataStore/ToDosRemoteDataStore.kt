package com.alirhn.assignment.data.dataStore

import com.alirhn.assignment.data.source.ToDosRemoteSource
import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos
import javax.inject.Inject

class ToDosRemoteDataStore @Inject constructor(
    private val toDosRemoteSource: ToDosRemoteSource
) : ToDosDataStore {
    override suspend fun getToDos(userId: String): ToDos {
        return  toDosRemoteSource.getToDos(userId);
    }

    override suspend fun login(username: String): LoginResponse {
        return toDosRemoteSource.login(username);
    }

}