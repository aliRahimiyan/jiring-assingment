package com.alirhn.assignment.data.dataStore

import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos

interface ToDosDataStore {

    suspend fun getToDos(userId : String) : ToDos

    suspend fun login(username : String) : LoginResponse
}