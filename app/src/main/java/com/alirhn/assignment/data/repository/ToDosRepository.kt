package com.alirhn.assignment.data.repository

import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos
import kotlinx.coroutines.flow.Flow

interface ToDosRepository {

    suspend fun getToDosList(userId : String): Flow<Result<ToDos>>

    suspend fun login(username : String) : Flow<Result<LoginResponse>>
}