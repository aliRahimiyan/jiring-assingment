package com.alirhn.assignment.utils

inline fun <T> apiCall(apiAction: () -> T): Result<T> {
    return runCatching { apiAction() }
}