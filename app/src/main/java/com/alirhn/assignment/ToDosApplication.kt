package com.alirhn.assignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ToDosApplication : Application() {
    override fun onCreate() {
        super.onCreate()
    }
}