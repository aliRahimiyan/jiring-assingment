package com.alirhn.assignment.presentation

import com.alirhn.assignment.baseClasses.BaseViewModel
import com.alirhn.assignment.data.repository.ToDosRepository
import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.ui.state.LoginUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class LoginViewModel  @Inject constructor(
    private  val repository: ToDosRepository,
) : BaseViewModel<LoginUiState>() {

    override val viewModelState: MutableStateFlow<LoginUiState> =
        MutableStateFlow(LoginUiState())
     fun login(username : String){
        getData(
            action = {
                repository.login(username)
            } ,
            onLoading = {
                loginLoading()
            },
            onSuccess = {data ->
                loginSuccess(data)
            },
            onError = {errorMessage ->
                loginError(errorMessage)
            }

        )
    }


    private fun loginLoading(){
        updateLoginLoading(isLoading = true)
        updateErrorLogin()
        updateSuccessLogin()
    }

    private fun loginError(errorMessage : String){
        updateLoginLoading(isLoading = false)
        updateErrorLogin(errorMessage =errorMessage)
        updateSuccessLogin()
    }

    private fun loginSuccess(data: LoginResponse){
        updateLoginLoading(isLoading = false)
        updateErrorLogin()
        updateSuccessLogin(data = data)
    }

    private fun updateLoginLoading(isLoading : Boolean){
        viewModelState.update {
            it.copy(
                loginResponse = it.loginResponse.copy(isLoading = isLoading)
            )
        }
    }

    private fun updateSuccessLogin(data : LoginResponse? = null){
        viewModelState.update {
            it.copy(
                loginResponse = it.loginResponse.copy(
                    data = data
                )
            )
        }
    }

    private fun updateErrorLogin(errorMessage : String ? = null){
        viewModelState.update {
            it.copy(
                loginResponse = it.loginResponse.copy(errorMessage = errorMessage)
            )
        }
    }

}