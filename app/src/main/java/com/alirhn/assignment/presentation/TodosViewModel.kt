package com.alirhn.assignment.presentation

import androidx.lifecycle.SavedStateHandle
import com.alirhn.assignment.baseClasses.BaseViewModel
import com.alirhn.assignment.data.repository.ToDosRepository
import com.alirhn.assignment.remote.model.ToDos
import com.alirhn.assignment.ui.component.navigation.TodosDestinationsArgs.USER_ID_ARG
import com.alirhn.assignment.ui.state.ToDosUIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class TodosViewModel @Inject constructor(
    private val toDosRepository: ToDosRepository,
    savedStateHandle : SavedStateHandle
) : BaseViewModel<ToDosUIState>() {
    private val userId: String? = savedStateHandle[USER_ID_ARG]
    override val viewModelState: MutableStateFlow<ToDosUIState> =
        MutableStateFlow(ToDosUIState())

    init {
        if (userId != null){
            getToDosList(userId)
        } else {
            updateToDossError("User id is null")
        }
    }
    private fun getToDosList(userId : String){
        getData(
            action = {
                toDosRepository.getToDosList(userId)
            } ,
            onLoading = {
                onGetToDossLoading()
            },
            onSuccess = {data ->
                onGetToDossSuccess(data)
            },
            onError = {errorMessage ->
                onGetToDossError(errorMessage)
            }

        )
    }


    private fun onGetToDossLoading(){
        updateToDossLoading(isLoading = true)
        updateToDossError()
        updateToDossSuccess()
    }

    private fun onGetToDossError(errorMessage : String){
        updateToDossLoading(isLoading = false)
        updateToDossError(errorMessage =errorMessage)
        updateToDossSuccess()
    }

    private fun onGetToDossSuccess(data: ToDos){
        updateToDossLoading(isLoading = false)
        updateToDossError()
        updateToDossSuccess(data = data)
    }

    private fun updateToDossLoading(isLoading : Boolean){
        viewModelState.update {
            it.copy(
                toDosList = it.toDosList.copy(isLoading = isLoading)
            )
        }
    }

    private fun updateToDossSuccess(data : ToDos? = null){
        viewModelState.update {
            it.copy(
                toDosList = it.toDosList.copy(
                    data = data
                )
            )
        }
    }

    private fun updateToDossError(errorMessage : String ? = null){
        viewModelState.update {
            it.copy(
                toDosList = it.toDosList.copy(errorMessage = errorMessage)
            )
        }
    }
}