package com.alirhn.assignment.baseClasses

data class BaseUIState<T>(
    val isLoading : Boolean = false,
    val errorMessage : String? = null ,
    val data : T? = null
)