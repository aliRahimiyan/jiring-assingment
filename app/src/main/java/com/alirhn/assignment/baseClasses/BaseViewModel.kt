package com.alirhn.assignment.baseClasses

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

abstract class BaseViewModel<T> : ViewModel() {
    protected abstract val viewModelState: MutableStateFlow<T>

    val uiState by lazy {
        viewModelState.stateIn(
            viewModelScope,
            SharingStarted.Eagerly,
            viewModelState.value
        )
    }

    protected fun <T : Any> getData(
        action: suspend () -> Flow<Result<T>>,
        onLoading: () -> Unit = {},
        onError: (String) -> Unit = {},
        onSuccess: (T) -> Unit = {}
    ) {
        viewModelScope.launch {
            onLoading()
            action().collect { result ->
                result.onSuccess {
                    result.getOrNull().let { data ->
                        if (data == null) {
                            onError("Data is Null!")
                        } else {
                            onSuccess(data)
                        }
                    }
                }
                result.onFailure {
                    result.exceptionOrNull().let { exception ->
                        if (exception != null) {
                            onError(exception.message ?: "Exception message is null")
                        }else {
                            onError("Exception is null")
                        }
                    }
                }

            }
        }
    }
}