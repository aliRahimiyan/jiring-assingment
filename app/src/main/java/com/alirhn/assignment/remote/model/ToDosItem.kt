package com.alirhn.assignment.remote.model


import com.google.gson.annotations.SerializedName

data class ToDosItem(
    @SerializedName("completed")
    val completed: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("userId")
    val userId: Int
)