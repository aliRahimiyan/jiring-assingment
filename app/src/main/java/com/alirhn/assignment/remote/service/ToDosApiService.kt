package com.alirhn.assignment.remote.service

import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos
import retrofit2.http.GET
import retrofit2.http.Query

interface ToDosApiService {

    @GET("/users")
    suspend fun login(@Query("username") username : String) : LoginResponse

    @GET("/todos")
    suspend fun getTodos(@Query("userId") userId : String) : ToDos
}