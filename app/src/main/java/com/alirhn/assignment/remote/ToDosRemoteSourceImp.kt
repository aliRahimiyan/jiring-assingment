package com.alirhn.assignment.remote

import com.alirhn.assignment.data.source.ToDosRemoteSource
import com.alirhn.assignment.remote.model.LoginResponse
import com.alirhn.assignment.remote.model.ToDos
import com.alirhn.assignment.remote.service.ToDosApiService
import javax.inject.Inject

class ToDosRemoteSourceImp @Inject constructor(private val toDosService: ToDosApiService) :
    ToDosRemoteSource {
    override suspend fun getToDos(userId: String): ToDos {
       val result = toDosService.getTodos(userId);
        return result;
    }

    override suspend fun login(username: String): LoginResponse {
        val result = toDosService.login(username);
        return result;
    }
}