package com.alirhn.assignment.injection


import com.alirhn.assignment.data.source.ToDosRemoteSource
import com.alirhn.assignment.remote.ToDosRemoteSourceImp
import com.alirhn.assignment.remote.service.ToDosApiService
import com.alirhn.assignment.utils.Constants.BASE_URL
import com.google.gson.Gson
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class RemoteModule {

    companion object {

        @Provides
        fun provideToDosApiService(
            gsonConverterFactory: GsonConverterFactory,
            okHttpClient: OkHttpClient,
        ): ToDosApiService {
            return Retrofit.Builder().baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
                .build().create(ToDosApiService::class.java)
        }
        @Singleton
        @Provides
        fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
            val logging = HttpLoggingInterceptor()

                HttpLoggingInterceptor.Level.BODY

            return logging
        }

        @Singleton
        @Provides
        fun provideOkHttpClient(
            httpLoggingInterceptor: HttpLoggingInterceptor
        ): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build()
        }
        @Provides
        fun provideGsonConverterFactory(): GsonConverterFactory {
            return GsonConverterFactory.create()
        }
        @Singleton
        @Provides
        fun provideGson(): Gson {
            return Gson()
        }

    }

    @Binds
    abstract fun bindToDosRemoteSource(
        toDosRemoteSourceImpl: ToDosRemoteSourceImp
    ): ToDosRemoteSource

}