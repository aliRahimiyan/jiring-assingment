package com.alirhn.assignment.injection

import com.alirhn.assignment.data.repository.ToDosRepository
import com.alirhn.assignment.data.repository.ToDosRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {

    @Binds
    abstract fun bindToDosRepository(repository: ToDosRepositoryImpl): ToDosRepository


}