# Android Project for Jiring Company

This README file provides an overview of the Android project developed for Jiring Company. The project is built using modern Android development practices, focusing on clean architecture, MVVM architecture, Dagger-Hilt for dependency injection, and Jetpack Compose for UI development.

## Table of Contents

- [Getting Started](#getting-started)
- [Built With](#built-with)
- [Architecture](#architecture)

## Getting Started

To get a local copy up and running, follow these simple steps.

1. Clone the repository
   ```
   git clone https://gitlab.com/aliRahimiyan/jiring-assingment.git
   ```
2. Open the project in Android Studio
3. Sync the project with Gradle files
4. Run the app on an emulator or a physical device

## Built With 🛠

- **Kotlin**: The first-class and official programming language for Android development.
- **Jetpack Compose**: Android’s modern toolkit for building native UI.
- **Flow**: A cold asynchronous data stream that sequentially emits values and completes normally or with an exception.
- **Android Architecture Components**: A collection of libraries that help you design robust, testable, and maintainable apps.
- **ViewModel**: Stores UI-related data that isn't destroyed on UI changes.
- **Hilt**: Easier way to incorporate Dagger DI into Android apps.
- **Retrofit**: A type-safe HTTP client for Android and Java.
- **Material Components for Android**: Modular and customizable Material Design UI components for Android.

## Architecture 🏗️

The project follows the MVVM (Model - View - ViewModel) architecture pattern, enhanced with clean architecture. Dependency injection is managed using Hilt, which simplifies the process of incorporating Dagger DI into Android apps.




## Contact

alirhn77@gmail.com

Project Link: https://gitlab.com/aliRahimiyan/jiring-assingment/-/tree/main?ref_type=heads

---
